# Realtime Chat (Mean Stack + Socket.io)

## Pre requisites

- Install [NodeJS](https://nodejs.org/)
- Install [MongoDB](<https://www.mongodb.org/>)
- Install [Gulp](http://gulpjs.com/>)

## Before

### Keep MongoDB running
```sh
mongod
```

Edit the ``.env`` file with your Mongo URL, if needed

### Install node modules
Open the project folder.
Install the node modules:
```sh
npm install
```

## Run

Keep the mongodb running in a tab and execute in another:
```sh
gulp
```
Open <http://localhost:3000> in your browser.
