var gulp        = require('gulp')
var jade        = require('gulp-jade')
var sass        = require('gulp-sass')
var concat      = require('gulp-concat')
var nodemon     = require('gulp-nodemon')
var babelify    = require('babelify')
var browserify  = require('browserify')
var source      = require('vinyl-source-stream')

var env = process.env.NODE_ENV || 'development'
var isProduction = env === 'production'

gulp.task('start', ['libs', 'jade', 'sass', 'build'], function () {
  nodemon({
    script: 'index.js',
    ext: 'html js',
    env: {'NODE_ENV': env}
  })
})

gulp.task('build', function () {
    return browserify('./src/client/client.js', {debug: !isProduction})
      .transform(babelify, {presets: ['es2015']})
      .bundle()
      .pipe(source('app.js'))
      .pipe(gulp.dest('public/js'))
})

gulp.task('libs', function () {
  return gulp
    .src([
      './node_modules/angular/angular.min.js',
      './node_modules/angular-route/angular-route.min.js',
      './node_modules/angular-resource/angular-resource.min.js',
      './node_modules/angular-cookies/angular-cookies.min.js',
      './node_modules/angular-gravatar/build/angular-gravatar.min.js',
      './node_modules/socket.io-client/socket.io.js',
      './node_modules/angular-socket-io/socket.min.js'
    ])
    .pipe(gulp.dest('./public/libs/'))
})

gulp.task('jade', function() {
  gulp
    .src('./src/client/views/**/*.jade')
    .pipe(jade())
    .pipe(gulp.dest(function(file) {
      //ignore file structure and save files in the same folder
      var otherFolders = file.relative.split('/')
      otherFolders.pop()
      file.base += otherFolders.join('/')

      return 'public/views/'
    }))
})

gulp.task('sass', ['views:stylesheets'], function () {
  gulp.src('./src/client/assets/stylesheets/**/*.sass')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./public/css'));
})

gulp.task('views:stylesheets', function () {
  return gulp
    .src('./src/client/views/**/*.sass')
    .pipe(concat('_views_auto_generated.sass'))
    .pipe(gulp.dest('./src/client/assets/stylesheets'))
})

gulp.task('watch', ['build', 'jade', 'sass'], function () {
  gulp.watch(['./src/client/**/*.js'], ['build'])
  gulp.watch(['./src/client/**/*.jade'], ['jade'])
  gulp.watch(['./src/client/**/*.sass'], ['sass'])
})

gulp.task('default', ['start', 'watch'])
