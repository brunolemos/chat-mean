var mongoose  = require('mongoose')
var app       = require('./src/server/server.js')
var routes    = require('./src/server/routes/routes')
var sockets   = require('./src/server/sockets/sockets')
var http      = require('http').Server(app)
var io        = require('socket.io')(http)

//connect to database (mongodb must already be running)
mongoose.connect(process.env.MONGO_URL)

//register routes
routes(app)

//setup socket server
sockets(io)

//start http server
http.listen(app.get('port'), function() {
  console.log('Running at localhost:' + app.get('port'))
})
