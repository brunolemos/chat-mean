export default angular.module('app',
  ['ngRoute', 'ngResource', 'ngCookies', 'ui.gravatar', 'btford.socket-io'])
