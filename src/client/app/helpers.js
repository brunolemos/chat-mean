import app from './app'
import './resources'

app.run(function($rootScope, User) {
  $rootScope.getUser = function(userId) {
    return User.get({id: userId}, function(user) {
      return user || {}
    })
  }
})
