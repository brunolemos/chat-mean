import app from './app'
import './services/CachedResource'

app.factory('User', function(CachedResource) {
  return CachedResource('/api/users/:id', {
    id: '@id'
  })
})

app.factory('Room', function(CachedResource) {
  return CachedResource('/api/rooms/:id', {
    id: '@id'
  })
})

app.factory('Message', function($resource) {
  return $resource('/api/rooms/:roomId/messages/:id', {
    id: '@id',
    roomId: '@roomId'
  })
})
