import app from './app'
import './services/AuthService'

app.config(function($routeProvider, $locationProvider) {
  $routeProvider
    .when('/', {
      name: 'main',
      controller: 'MainController',
      templateUrl: '/views/main.html'
    })

    .when('/login', {
      name: 'login',
      controller: 'LoginController',
      templateUrl: '/views/login.html',
      onlyPublic: true
    })

    .when('/signup', {
      name: 'signup',
      controller: 'SignupController',
      templateUrl: '/views/signup.html',
      onlyPublic: true
    })

    .when('/logout', {
      name: 'logout',
      resolve: {
        logout: function ($location, AuthService) {
          AuthService.logout()
          $location.path('/')
        }
      },
    })

    .when('/rooms', {
      name: 'rooms',
      controller: 'RoomsController',
      templateUrl: '/views/rooms.html',
      isPrivate: true
    })

    .when('/rooms/new', {
      name: 'newRoom',
      controller: 'NewRoomController',
      templateUrl: '/views/newRoom.html',
      isPrivate: true
    })

    .when('/rooms/:id', {
      name: 'room',
      controller: 'RoomController',
      templateUrl: '/views/room.html',
      isPrivate: true
    })

    .otherwise({
      redirectTo: '/login'
    })

  $locationProvider.html5Mode({enabled: true, requireBase: false})
})

.run(function($rootScope, $location, AuthService) {
  $rootScope.$on('$routeChangeStart', function(e, next, current) {

    if(next.isPrivate && !AuthService.isAuthenticated()) {
        if(!current || current.name != 'login')
          $location.path('/login')
    } else if (next.onlyPublic && AuthService.isAuthenticated()) {
        if(!current || current.name != 'main')
          $location.path('/')
    }
  })
})
