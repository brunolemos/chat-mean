import app from '../app'

app.factory('AppCache', function($cacheFactory) {
  return $cacheFactory('app')
})
