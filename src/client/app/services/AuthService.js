import app from '../app'
import '../resources'

app.factory('AuthService', function($http, $location, $cookies, User) {
  var authService = {}

  authService.getUser = function() {
      return $cookies.getObject('user') || {}
  }

  authService.getUserId = function() {
      return authService.getUser()._id
  }

  authService.isAuthenticated = function() {
      return !!authService.getUserId()
  }

  authService.logout = function() {
    $http.post('/api/auth/logout')
    $cookies.remove('user')
  }

  authService.login = function(data, callback) {
    $http.post('/api/auth', data)
      .success(function(res) {
        authCallback(res, callback)
      })
      .error(function(res) {
        authCallback(res, callback)
      })
  }

  authService.signup = function(user, callback) {
    User.save(user, function(res) {
      authCallback(res, callback)
    }, function(res) {
      authCallback(res.data, callback)
    })
  }

  var authCallback = function(res, callback) {
    if(res.data)
      $cookies.putObject('user', res.data)
    else
      authService.logout()

    callback(res)
  }

  return authService
})
