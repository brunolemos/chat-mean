import app from '../app'
import './AppCache'

app.factory('CachedResource', function($resource, AppCache) {
  var interceptSave = {
    response: function(response) {
      //invalidate cache
      AppCache.remove(response.config.url)

      return response.data
    }
  }

  var interceptDelete = {
    response: function(response) {
      var urlArr = response.config.url.split('/')
      urlArr.pop()

      //invalidate cache
      var url = urlArr.join('/')
      AppCache.remove(url)

      return response.data
    }
  }

  return function(url, paramDefaults, actions, options) {
    actions = angular.merge({
      'get':    { method: 'GET', cache: AppCache },
      'query':  { method: 'GET', cache: AppCache, isArray: true },
      'save':   { method: 'POST', interceptor: interceptSave },
      'remove': { method: 'DELETE', interceptor: interceptDelete },
      'delete': { method: 'DELETE', interceptor: interceptDelete },
    }, actions)

    return $resource(url, paramDefaults, actions, options)
  }
})
