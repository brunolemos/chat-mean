import app from '../app'

app.factory('ChatSocket', function(socketFactory) {
  var ChatSocket = socketFactory()
  ChatSocket.forward('message')

  return ChatSocket
})
