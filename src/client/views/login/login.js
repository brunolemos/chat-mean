import md5 from 'md5'
import app from '../../app/app'
import '../../app/services/AuthService'

app.controller('LoginController', function($scope, $location, AuthService) {
  $scope.login = function() {
    var data = {
      'email': $scope.email,
      'password': md5($scope.password)
    }

    AuthService.login(data, function(res) {
      if(res.error) {
        alert(res.error.message)
        return
      }

      $location.path('/')
    })
  }
})
