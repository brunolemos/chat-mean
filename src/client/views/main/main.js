import app from '../../app/app'
import '../../app/services/AuthService'

app.controller('MainController', function($scope, $location, AuthService) {
  if(AuthService.isAuthenticated())
    $location.path('/rooms')
  else
    $location.path('/login')
})
