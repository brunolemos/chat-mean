import app from '../../app/app'
import '../../app/services/AuthService'
import '../../app/services/AppCache'
import '../../app/services/ChatSocket'
import '../../app/resources'

app.controller('RoomController', function($rootScope, $scope, $routeParams, $location, AuthService, AppCache, ChatSocket, Room, Message) {
  $scope.getUser = $rootScope.getUser

  $scope.room = {}
  $scope.room.messages = []

  $scope.$on('socket:message', function(event, message) {
    console.log('message:received', message)

    if(message.roomId != $scope.room._id) return

    $scope.room.messages.push(message)
    $scope.updateRoomCache()
  })

  $scope.$on('$destroy', function handler() {
    ChatSocket.emit('room:leave', $scope.room._id)
  })

  Room.get({id: $routeParams.id}, function(room) {
    if(!room || !room._id)
      $location.path('/rooms')

    $scope.room = room || {}
    $scope.room.messages = $scope.room.messages || []
    ChatSocket.emit('room:join', $scope.room._id)

    //get remaining messages (TODO: Fix it (only returning one) + Update cache)
    var lastMessage = $scope.room.messages.length > 0 ? $scope.room.messages[$scope.room.messages.length - 1] : {}
    ChatSocket.emit('room:getMessages', $scope.room._id, lastMessage ? lastMessage.createdAt : null)
  })

  $scope.deleteRoom = function(id) {
    Room.delete({id: id}, function(res) {
      $scope.invalidateRoomsCache()
      $location.path('/rooms')
    })
  }

  $scope.submit = function(text) {
    var message = new Message()
    message.roomId = $scope.room._id
    message.userId = AuthService.getUserId()
    message.text = text

    Message.save(message, function(res) {
      // console.log('Message.save', res)

      if(res.error) {
        alert(res.error.message)
        return
      }

      $scope.room.messages.push(res.data)
      $scope.message = ''

      ChatSocket.emit('message', message.roomId, message.userId, message.text)
      $scope.scrollToBottom()
      $scope.updateRoomCache()
    })
  }

  $scope.$watchCollection('room.messages', function(newMessages, oldMessages) {
    //will scroll to bottom if is almost scrolled to bottom
    var container = document.getElementById('messages-container')
    var diff = container.scrollHeight - container.scrollTop

    if(diff - container.clientHeight <= 100)
      $scope.scrollToBottom()
  })

  $scope.messageClass = function(message) {
    return AuthService.getUserId() == message.userId ? 'sent' : 'received'
  }

  $scope.scrollToBottom = function() {
    setTimeout(function() {
      var container = document.getElementById('messages-container')
      container.scrollTop = container.scrollHeight

      var classNames = container.className.split(' ')
      if(classNames.indexOf('loaded') >= 0) return

      classNames.push('loaded')
      container.className = classNames.join(' ')
    }, 10)
  }

  $scope.updateRoomCache = function() {
    if(!$scope.room._id) return
    AppCache.put('/api/rooms/' + $scope.room._id, $scope.room)
  }

  $scope.invalidateRoomsCache = function() {
    if(!$scope.room._id) return
    AppCache.remove('/api/rooms')
  }
})
