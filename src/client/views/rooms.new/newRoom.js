import app from '../../app/app'
import '../../app/resources'

app.controller('NewRoomController', function($scope, $location, Room) {
  $scope.submit = function(newRoom) {
    var room = new Room()
    room.name = newRoom.name
    room.description = newRoom.roomDescription

    Room.save(room, function(room) {
      $location.path('/rooms/' + (room._id || ''))

    }, function(res) {
      alert(res.data.error.message || 'Failed to create room')
    })
  }
})
