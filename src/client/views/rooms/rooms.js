import app from '../../app/app'
import '../../app/resources'

app.controller('RoomsController', function($scope, $location, Room) {
  $scope.rooms = []

  Room.query(function(rooms) {
    $scope.rooms = rooms || []
  })
})
