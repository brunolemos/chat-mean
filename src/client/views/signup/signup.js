import md5 from 'md5'
import app from '../../app/app'
import '../../app/services/AuthService'
import '../../app/resources'

app.controller('SignupController', function($scope, $location, AuthService, User) {
  $scope.signup = function() {
    if(!this.validate()) return

    var user = new User()
    user.name = $scope.name
    user.email = $scope.email
    user.password = md5($scope.password)

    AuthService.signup(user, function(res) {
      if(res.error) {
        alert(res.error.message)
        return
      }

      $location.path('/')
    })
  }

  $scope.validate = function() {
    if($scope.password.length < 3) {
      alert('Password must have at least 3 characters')
      return false

    } else if($scope.password != $scope.confirm_password) {
      alert('Passwords don\'t match')
      return false
    }

    return true
  }
})
