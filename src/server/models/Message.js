var mongoose    = require('mongoose')
var timestamps  = require('mongoose-timestamp')
var ObjectId    = mongoose.Schema.Types.ObjectId

var MessageSchema = new mongoose.Schema({
  roomId: { type: ObjectId, ref: 'Room', required: true },
  userId: { type: ObjectId, ref: 'User', required: true },
  text: { type: String, required: true }
})

MessageSchema.plugin(timestamps)

module.exports.schema = MessageSchema
module.exports.model = mongoose.model('Message', MessageSchema)
