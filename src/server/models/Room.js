var mongoose    = require('mongoose')
var timestamps  = require('mongoose-timestamp')

var RoomSchema = new mongoose.Schema({
  name: {type: String, required: true, unique: true},
  description: String
})

RoomSchema.plugin(timestamps)

module.exports.schema = RoomSchema
module.exports.model = mongoose.model('Room', RoomSchema)
