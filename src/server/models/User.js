var mongoose    = require('mongoose')
var timestamps  = require('mongoose-timestamp')
var bcrypt      = require('bcrypt-nodejs')

var UserSchema = new mongoose.Schema({
  name: String,
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true, select: false }
})

UserSchema.plugin(timestamps)

UserSchema.methods.verifyPassword = function(password) {
  return bcrypt.compareSync(password, this.password)
}

module.exports.schema = UserSchema
module.exports.model = mongoose.model('User', UserSchema)
