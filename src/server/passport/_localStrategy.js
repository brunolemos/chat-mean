var passport      = require('passport')
var LocalStrategy = require('passport-local').Strategy
var User          = require('../models/User').model

passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    session: true
  },

  function(email, password, done) {
    User.findOne({email: email}, '+password', function(err, user) {
      if(err)
        return done(err)

      if(!user || !user.verifyPassword(password))
        return done(null, false, 'E-mail or password incorrect')

      return done(null, user)
    })
  }
))
