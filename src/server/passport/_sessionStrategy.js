var passport        = require('passport')
var SessionStrategy = require('passport-custom')

passport.use('_session', new SessionStrategy(
  function(req, done) {
    done(null, req.user || false)// || {_id: '56ba7f3c533a54042930a7d8'}
  }
))
