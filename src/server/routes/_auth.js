var passport 	= require('passport')
var bcrypt 		= require('bcrypt-nodejs')
var User    	= require('../models/User').model

module.exports = function(app) {
	app.post('/api/auth', function(req, res, next) {
		passport.authenticate('local', function(err, user, message) {
			var message = message ? (message.message ? message.message : message) : ''

			if (err) return next(err)
	    if (message) return res.status(400).json({error: {message: message}})

			req.login(user, function(err) {
	      if (err) return next(err)

				delete req.user.password
				return res.json({data: req.user})
    	})
		})(req, res, next)
	})

	app.post('/api/auth/logout', passport.authenticate('session'), function(req, res, next) {
		req.logout()
		req.session.destroy()

  	res.json({data: true})
	})
}
