module.exports = function(app) {
	//all routes without extension
	app.get(['/', /^((?!api).)*[^.]{5}$/], function(req, res) {
		res.render('index')
	})
}
