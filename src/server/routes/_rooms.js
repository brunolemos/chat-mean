var passport	= require('passport')
var bcrypt  	= require('bcrypt-nodejs')
var Room    	= require('../models/Room').model
var Message  	= require('../models/Message').model

module.exports = function(app) {
	app.get('/api/rooms', passport.authenticate('_session'), function(req, res) {
		Room.find(req.query, function(err, rooms) {
			if(err || !rooms) res.status(400).json([])
      res.json(rooms)
		})
	})

	app.post('/api/rooms', passport.authenticate('_session'), function(req, res) {
    var room = new Room({
      name: req.body.name,
      description: req.body.description
    })

    room.save(function(err, room) {
			if(err || !room)
				return res.status(400).json({
					error: {message: err.message}
				})

      res.json(room)
    })
	})

	app.get('/api/rooms/:id', passport.authenticate('_session'), function(req, res) {
		Room.findOne({_id: req.params.id}, function(err, room) {
			if(err || !room) res.status(400).json({})
			if(req.query.messages == '0') return res.json(room)

      Message.find({roomId: req.params.id}, function(err, messages) {
				var result = JSON.parse(JSON.stringify(room))
				result.messages = messages || []

				res.json(result)
			})
		})
	})

	app.delete('/api/rooms/:id', passport.authenticate('_session'), function(req, res) {
		Room.remove({_id: req.params.id}, function(err, room) {
			if(err || !room) res.status(400).json({})

			Message.remove({roomId: req.params.id}, function(err, messages) {
				res.json(room)
			})
		})
	})
}
