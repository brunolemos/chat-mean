var passport	= require('passport')
var bcrypt  	= require('bcrypt-nodejs')
var Room    	= require('../models/Room').model
var Message   = require('../models/Message').model

module.exports = function(app) {
	app.get('/api/rooms/:roomId/messages', passport.authenticate('_session'), function(req, res) {
		Message.find({roomId: req.params.roomId}, function(err, messages) {
      res.json(messages || [])
		})
	})

	app.post('/api/rooms/:roomId/messages', passport.authenticate('_session'), function(req, res) {
		var message = new Message({
			roomId: req.params.roomId,
			userId: req.user._id,
			text: req.body.text
		})

    message.save(function(err, message) {
      if (err)
				return res.status(400).json({
					error: {message: err.message || err.errmsg || 'Failed to save message'}
				})

      res.json({data: message})
		})
	})

	app.get('/api/rooms/:roomId/messages/:id', passport.authenticate('_session'), function(req, res) {
		Message.findById(req.params.id, {roomId: req.params.roomId}, function(err, message) {
      res.json(message || {})
		})
	})

	app.delete('/api/rooms/:roomId/messages/:id', passport.authenticate('_session'), function(req, res) {
		Message.findOneAndRemove([{_id: req.params.id, roomId: req.params.roomId}], function(err, message) {
      if (err)
				return res.status(400).json({
					error: {message: err.message || err.errmsg || 'Failed to remove message'}
				})

      res.json({data: message})
		})
	})
}
