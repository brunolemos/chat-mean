var passport	= require('passport')
var bcrypt  	= require('bcrypt-nodejs')
var User    	= require('../models/User').model

module.exports = function(app) {
	app.post('/api/users', function(req, res) {
    var user = new User({
      name: req.body.name,
      email: req.body.email,
      password: bcrypt.hashSync(req.body.password)
    })

    user.save(function(err, user) {
      if (err)
				return res.status(400).json({
					error: {message: err.message || err.errmsg}
				})

			req.login(user, function(err) {
	      if (err) return next(err)

				delete req.user.password
				return res.json({data: req.user})
    	})
    })
	})

	app.get('/api/users/:id', passport.authenticate('_session'), function(req, res) {
		User.findOne({_id: req.params.id}, function(err, user) {
			res.json(user || {})
		})
	})
}
