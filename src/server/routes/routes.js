module.exports = function(app) {
  require('./_auth')(app)
  require('./_users')(app)
  require('./_rooms')(app)
  require('./_rooms.messages')(app)
	require('./_index')(app)
}
