var express     = require('express')
var session     = require('express-session')
var passport    = require('passport')
var bodyParser  = require('body-parser')
var path        = require('path')
var dotenv      = require('dotenv').config()
var app         = express()
require('./passport/passport')

app.set('view engine', 'jade')
app.set('views', path.join(__dirname, '../client/views'))
app.set('port', (process.env.PORT || 3000))

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(express.static(path.join(__dirname, '../../public')))

app.use(session({resave: false, saveUninitialized: false, secret: 'chat123#$'}))
app.use(passport.initialize())
app.use(passport.session())

module.exports = app
