var Message = require('../models/Message').model

module.exports = function(io) {
  io.on('connection', function(socket) {
    socket.on('room:join', function(roomId) {
      console.log('room:join', roomId)
      socket.join(roomId)
    })

    socket.on('room:leave', function(roomId) {
      console.log('room:leave', roomId)
      socket.leave(roomId)
    })

    socket.on('room:getMessages', function(roomId, afterDate) {
      console.log('room:getMessages', roomId, afterDate)

      var query = {roomId: roomId}

      if(afterDate)
        query.createdAt = {$gt: new Date(afterDate)}

      Message.find(query, function(err, messages) {
        var messages = messages || []

        for(message of messages)
          socket.emit('message', message)
  		})
    })

    socket.on('message', function(roomId, userId, message) {
      console.log('message from', userId, 'to', roomId, message)
      socket.broadcast.to(roomId).emit('message', {
        roomId: roomId,
        userId: userId,
        text: message
      })
    })
  })
}
